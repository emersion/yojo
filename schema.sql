CREATE TABLE Installation (
	id INTEGER PRIMARY KEY,
	webhook_token TEXT NOT NULL,
	srht_token INTEGER NOT NULL REFERENCES OAuthToken(id),
	gitea_token INTEGER NOT NULL REFERENCES OAuthToken(id),
	created_at datetime NOT NULL,
	secrets TEXT NOT NULL DEFAULT ''
);

CREATE UNIQUE INDEX InstallationWebhookTokenIndex ON Installation(webhook_token);

CREATE TABLE OAuthToken (
	id INTEGER PRIMARY KEY,
	issuer TEXT NOT NULL,
	username TEXT NOT NULL,
	access_token TEXT NOT NULL,
	refresh_token TEXT,
	created_at datetime NOT NULL,
	expires_at datetime,
	scope TEXT
);

CREATE UNIQUE INDEX OAuthTokenIndex ON OAuthToken(issuer, username);
