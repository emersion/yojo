package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"path"
	"strings"
	"time"

	"code.gitea.io/sdk/gitea"
	"git.sr.ht/~emersion/gqlclient"
	"gopkg.in/yaml.v3"

	"codeberg.org/emersion/yojo/buildssrht"
)

type buildContext struct {
	context.Context

	srht  *gqlclient.Client
	gitea *gitea.Client

	allowedSecrets []string

	baseRepo *giteaRepository
	headRepo *giteaRepository
	headHash string
	headRef  string // may be empty

	// these might be nil
	commit      *giteaCommit
	pullRequest *giteaPullRequest
}

func newBuildContext(ctx context.Context, installation *Installation) (*buildContext, error) {
	srhtClient, err := newSrhtClient(ctx, &installation.SrhtToken)
	if err != nil {
		return nil, fmt.Errorf("failed to create sr.ht client: %v", err)
	}

	giteaClient, err := newGiteaClient(ctx, &installation.GiteaToken)
	if err != nil {
		return nil, fmt.Errorf("failed to create Gitea client: %v", err)
	}

	return &buildContext{
		Context: ctx,
		srht:    srhtClient,
		gitea:   giteaClient,
	}, nil
}

func handleCommit(ctx context.Context, event *giteaCommitEvent, installation *Installation) error {
	buildCtx, err := newBuildContext(ctx, installation)
	if err != nil {
		return err
	}

	buildCtx.baseRepo = event.Repository
	buildCtx.headRepo = event.Repository
	buildCtx.headHash = event.HeadCommit.ID
	buildCtx.headRef = event.Ref
	buildCtx.commit = event.HeadCommit
	buildCtx.allowedSecrets = installation.Secrets

	return startAllJobs(buildCtx)
}

func handlePullRequest(ctx context.Context, event *giteaPullRequestEvent, installation *Installation) error {
	if event.Action != "opened" && event.Action != "reopened" && event.Action != "synchronized" {
		return nil
	}

	pr := event.PullRequest
	if pr.Head.Repository.FullName == pr.Base.Repository.FullName {
		return nil
	}

	buildCtx, err := newBuildContext(ctx, installation)
	if err != nil {
		return err
	}

	buildCtx.baseRepo = pr.Base.Repository
	buildCtx.headRepo = pr.Head.Repository
	buildCtx.headHash = pr.Head.SHA
	buildCtx.pullRequest = pr

	return startAllJobs(buildCtx)
}

func startAllJobs(ctx *buildContext) error {
	filenames, err := fetchManifestFilenames(ctx.gitea, ctx.headRepo.Owner.Username, ctx.headRepo.Name, ctx.headHash)
	if err != nil {
		return fmt.Errorf("failed to fetch build manifests: %v", err)
	}

	// Select a few manifests at random if there are too many
	if len(filenames) > maxJobsPerCommit {
		rand.Shuffle(len(filenames), func(i, j int) {
			filenames[i], filenames[j] = filenames[j], filenames[i]
		})
		filenames = filenames[:maxJobsPerCommit]
	}

	for _, filename := range filenames {
		if err := startJob(ctx, filename); err != nil {
			return fmt.Errorf("failed to submit build job for manifest %q: %v", filename, err)
		}
	}

	return nil
}

func startJob(ctx *buildContext, filename string) error {
	manifest, err := fetchManifest(ctx.gitea, ctx.headRepo.Owner.Username, ctx.headRepo.Name, ctx.headHash, filename)
	if err != nil {
		return fmt.Errorf("failed to fetch manifest: %v", err)
	} else if manifest == nil {
		return nil
	}

	basename := path.Base(filename)
	name := strings.TrimSuffix(basename, path.Ext(basename))
	if filename == ".build.yml" {
		name = ""
	}

	sourcesIface, ok := manifest["sources"]
	if ok {
		cloneURL, err := url.Parse(ctx.headRepo.CloneURL)
		if err != nil {
			return fmt.Errorf("failed to parse Gitea clone URL: %v", err)
		}

		manifestCloneURL := *cloneURL
		manifestCloneURL.Fragment = ctx.headHash

		sources, ok := sourcesIface.([]interface{})
		if !ok {
			return fmt.Errorf("invalid manifest: `sources` is not a list")
		}

		for i, srcIface := range sources {
			src, ok := srcIface.(string)
			if !ok {
				return fmt.Errorf("invalid manifest: `sources` contains a %T, want a string", srcIface)
			}

			// TODO: use fork parent to figure out whether we should replace
			// the source
			if strings.HasSuffix(src, "/"+ctx.baseRepo.Name) || strings.HasSuffix(src, "/"+ctx.baseRepo.Name+".git") {
				sources[i] = manifestCloneURL.String()
			}
		}
	}

	envIface, ok := manifest["environment"]
	if !ok {
		envIface = make(map[string]interface{})
		manifest["environment"] = envIface
	}
	env, ok := envIface.(map[string]interface{})
	if !ok {
		return fmt.Errorf("invalid manifest: `environment` is not a map with string keys")
	}
	env["BUILD_SUBMITTER"] = "yojo"
	if ctx.headRef != "" {
		env["GIT_REF"] = ctx.headRef
	}

	requestedSecretsIface, ok := manifest["secrets"]
	if !ok {
		requestedSecretsIface = []interface{}(nil)
	}
	requestedSecrets, ok := requestedSecretsIface.([]interface{})
	if !ok {
		return fmt.Errorf("invalid manifest: `secrets` is not a list")
	}
	var secrets []string
	for _, reqUUIDIface := range requestedSecrets {
		reqUUID, ok := reqUUIDIface.(string)
		if !ok {
			return fmt.Errorf("invalid manifest: `secrets` is not a list of strings")
		}
		for _, allowedUUID := range ctx.allowedSecrets {
			if reqUUID == allowedUUID {
				secrets = append(secrets, reqUUID)
				break
			}
		}
	}
	manifest["secrets"] = secrets

	delete(manifest, "oauth")

	manifestBuf, err := yaml.Marshal(manifest)
	if err != nil {
		return fmt.Errorf("failed to marshal manifest: %v", err)
	}

	var branch string
	if strings.HasPrefix(ctx.headRef, "refs/heads/") {
		branch = strings.TrimPrefix(ctx.headRef, "refs/heads/")
	}

	tags := []string{ctx.baseRepo.Name}
	if ctx.pullRequest != nil {
		tags = append(tags, "pulls", fmt.Sprintf("%v", ctx.pullRequest.Number))
	} else if branch != "" {
		tags = append(tags, "commits", branch)
	}
	if name != "" {
		tags = append(tags, name)
	}

	var note string
	if pr := ctx.pullRequest; pr != nil {
		note = fmt.Sprintf(`%v

[#%v] — %v

[#%v]: %v`, pr.Title, pr.Number, pr.User.Username, pr.Number, pr.URL)
	} else if commit := ctx.commit; commit != nil {
		title, _, _ := strings.Cut(commit.Message, "\n")
		shortHash := commit.ID[0:10]
		note = fmt.Sprintf(`%v

[%v] — %v

[%v]: %v`, title, shortHash, commit.Author.Name, shortHash, commit.URL)
	} else {
		panic("unreachable")
	}

	visibility := buildssrht.VisibilityPublic
	if ctx.headRepo.Private {
		visibility = buildssrht.VisibilityPrivate
	} else if ctx.headRepo.Internal {
		visibility = buildssrht.VisibilityUnlisted
	}

	enableSecretsValue := false
	enableSecrets := &enableSecretsValue
	if len(secrets) > 0 {
		enableSecrets = nil // automatic
	}

	job, err := buildssrht.SubmitJob(ctx.srht, ctx, string(manifestBuf), enableSecrets, tags, &note, &visibility)
	if err != nil {
		return fmt.Errorf("failed to submit sr.ht job: %v", err)
	}

	statusContext := "builds.sr.ht"
	if name != "" {
		statusContext += "/" + name
	}

	setCommitStatus(ctx, job, statusContext, gitea.StatusPending, "Build started…")
	if err != nil {
		return err
	}

	monitorWaitGroup.Add(1)
	go func() {
		defer monitorWaitGroup.Done()

		// Shallow copy build context and Gitea client to assign a different
		// deadline
		buildCtx := *ctx
		giteaClient := *ctx.gitea
		buildCtx.gitea = &giteaClient

		ctx, cancel := context.WithTimeout(monitorContext, 6*time.Hour)
		defer cancel()

		buildCtx.Context = ctx
		buildCtx.gitea.SetContext(ctx)

		if err := monitorJob(&buildCtx, job, statusContext); err != nil {
			setCommitStatus(&buildCtx, job, statusContext, gitea.StatusFailure, "Internal error")
		}
	}()

	return nil
}

func monitorJob(ctx *buildContext, submittedJob *buildssrht.Job, statusContext string) error {
	prevStatus := buildssrht.JobStatusPending
	for {
		time.Sleep(monitorJobInterval)

		var (
			job *buildssrht.Job
			err error
		)
		for i := 0; job == nil && i < monitorMaxRetries; i++ {
			job, err = buildssrht.FetchJob(ctx.srht, ctx, submittedJob.Id)
			if err != nil {
				log.Printf("failed to fetch sr.ht job #%v (try %v/%v): %v", submittedJob.Id, i+1, monitorMaxRetries, err)
				job = nil
				time.Sleep(monitorJobInterval)
			}
		}
		if err != nil {
			return fmt.Errorf("failed to fetch sr.ht job: %v", err)
		}

		if job.Status == prevStatus {
			continue
		}

		state, description := jobStatusToGitea(job.Status)
		if err := setCommitStatus(ctx, submittedJob, statusContext, state, description); err != nil {
			log.Printf("failed to update Gitea commit status: %v", err)
		}

		switch job.Status {
		case buildssrht.JobStatusPending, buildssrht.JobStatusQueued, buildssrht.JobStatusRunning:
			// Continue
		default:
			return nil
		}
	}
}

func setCommitStatus(ctx *buildContext, job *buildssrht.Job, statusContext string, state gitea.StatusState, desc string) error {
	// TODO: set timeout to 15s
	detailsURL := fmt.Sprintf("%v/%v/job/%v", config.Srht.BuildsEndpoint, job.Owner.CanonicalName, job.Id)
	_, _, err := ctx.gitea.CreateStatus(ctx.baseRepo.Owner.Username, ctx.baseRepo.Name, ctx.headHash, gitea.CreateStatusOption{
		State:       state,
		TargetURL:   detailsURL,
		Description: desc,
		Context:     statusContext,
	})
	if err != nil {
		return fmt.Errorf("failed to set Gitea commit status: %v", err)
	}
	return nil
}

func jobStatusToGitea(jobStatus buildssrht.JobStatus) (state gitea.StatusState, description string) {
	switch jobStatus {
	case buildssrht.JobStatusPending:
		return gitea.StatusPending, "Job pending…"
	case buildssrht.JobStatusQueued:
		return gitea.StatusPending, "Job queued…"
	case buildssrht.JobStatusRunning:
		return gitea.StatusPending, "Job running…"
	case buildssrht.JobStatusSuccess:
		return gitea.StatusSuccess, "Job completed"
	case buildssrht.JobStatusFailed:
		return gitea.StatusError, "Job failed"
	case buildssrht.JobStatusTimeout:
		return gitea.StatusFailure, "Job timed out"
	case buildssrht.JobStatusCancelled:
		return gitea.StatusFailure, "Job cancelled"
	default:
		panic(fmt.Sprintf("unknown sr.ht job status: %v", jobStatus))
	}
}
