package main

import (
	"os"

	"git.sr.ht/~emersion/go-scfg"
)

type Config struct {
	Listen string `scfg:"listen"`
	DB     string `scfg:"db"`

	Srht struct {
		ClientID     string `scfg:"client-id"`
		ClientSecret string `scfg:"client-secret"`

		BuildsEndpoint string `scfg:"builds-endpoint"`
		MetaEndpoint   string `scfg:"meta-endpoint"`
		Name           string `scfg:"name"`
	} `scfg:"srht"`

	Gitea struct {
		ClientID     string `scfg:"client-id"`
		ClientSecret string `scfg:"client-secret"`

		Endpoint string `scfg:"endpoint"`
		Name     string `scfg:"name"`
	} `scfg:"gitea"`
}

func loadConfig(filename string) (*Config, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var config Config
	err = scfg.NewDecoder(f).Decode(&config)
	return &config, err
}
