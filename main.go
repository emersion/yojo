package main

import (
	"context"
	"embed"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"log"
	"mime"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"code.gitea.io/sdk/gitea"
	"git.sr.ht/~emersion/go-oauth2"
	"git.sr.ht/~emersion/gqlclient"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"codeberg.org/emersion/yojo/buildssrht"
)

var (
	//go:embed templates
	templateFS embed.FS
	//go:embed static
	staticFS embed.FS
)

const (
	monitorJobInterval = 5 * time.Second
	monitorMaxRetries  = 10
	maxJobsPerCommit   = 4
)

var (
	srhtAuthScope    = []string{"builds.sr.ht/PROFILE:RO", "builds.sr.ht/JOBS:RW"}
	srhtSecretsScope = "builds.sr.ht/SECRETS:RO"
	giteaAuthScope   = []string{"repo:status", "admin:repo_hook", "admin:user_hook", "admin:org_hook"}
)

var (
	config              *Config
	srhtAuth, giteaAuth *oauth2.Client
	db                  *DB
	webhookEndpoint     string
)

var (
	monitorContext   context.Context
	monitorWaitGroup sync.WaitGroup
)

func main() {
	var configFilename, listenFlag, dbFlag string
	flag.StringVar(&configFilename, "config", "", "Configuration file")
	flag.StringVar(&listenFlag, "listen", "", "HTTP server listening address")
	flag.StringVar(&dbFlag, "db", "", "database path")
	flag.StringVar(&webhookEndpoint, "webhook-endpoint", "", "Web hook endpoint URL")
	flag.Parse()

	config = new(Config)
	if configFilename != "" {
		var err error
		config, err = loadConfig(configFilename)
		if err != nil {
			log.Fatalf("failed to load config file: %v", err)
		}
	}

	if listenFlag != "" {
		config.Listen = listenFlag
	}
	if dbFlag != "" {
		config.DB = dbFlag
	}

	if config.Srht.ClientID == "" {
		config.Srht.ClientID = os.Getenv("SRHT_CLIENT_ID")
	}
	if config.Srht.ClientSecret == "" {
		config.Srht.ClientSecret = os.Getenv("SRHT_CLIENT_SECRET")
	}
	if config.Gitea.ClientID == "" {
		config.Gitea.ClientID = os.Getenv("GITEA_CLIENT_ID")
	}
	if config.Gitea.ClientSecret == "" {
		config.Gitea.ClientSecret = os.Getenv("GITEA_CLIENT_SECRET")
	}

	if config.Listen == "" {
		config.Listen = "localhost:8080"
	}
	if config.DB == "" {
		config.DB = "yojo.db"
	}
	if config.Srht.BuildsEndpoint == "" {
		config.Srht.BuildsEndpoint = "https://builds.sr.ht"
	}
	if config.Srht.MetaEndpoint == "" {
		config.Srht.MetaEndpoint = "https://meta.sr.ht"
	}
	if config.Gitea.Endpoint == "" {
		config.Gitea.Endpoint = "https://codeberg.org"
	}

	if config.Srht.ClientID == "" || config.Srht.ClientSecret == "" {
		log.Fatal("missing sr.ht client ID and secret")
	}
	if config.Gitea.ClientID == "" || config.Gitea.ClientSecret == "" {
		log.Fatal("missing Gitea client ID and secret")
	}

	u, err := url.Parse(config.Gitea.Endpoint)
	if err != nil {
		log.Fatalf("invalid Gitea endpoint: %v", err)
	}
	giteaHostname := u.Host

	if config.Srht.Name == "" {
		config.Srht.Name = "sr.ht"
	}
	if config.Gitea.Name == "" {
		config.Gitea.Name = giteaHostname
	}

	srhtAuth = &oauth2.Client{
		Server: &oauth2.ServerMetadata{
			AuthorizationEndpoint: config.Srht.MetaEndpoint + "/oauth2/authorize",
			TokenEndpoint:         config.Srht.MetaEndpoint + "/oauth2/access-token",
		},
		ClientID:     config.Srht.ClientID,
		ClientSecret: config.Srht.ClientSecret,
	}
	giteaAuth = &oauth2.Client{
		Server: &oauth2.ServerMetadata{
			AuthorizationEndpoint: config.Gitea.Endpoint + "/login/oauth/authorize",
			TokenEndpoint:         config.Gitea.Endpoint + "/login/oauth/access_token",
		},
		ClientID:     config.Gitea.ClientID,
		ClientSecret: config.Gitea.ClientSecret,
	}

	db, err = openDB(config.DB)
	if err != nil {
		log.Fatalf("failed to open DB: %v", err)
	}

	tpl := template.Must(template.ParseFS(templateFS, "templates/*.html"))

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(60 * time.Second))
	r.Use(sessionMiddleware)

	r.Handle("/static/*", http.FileServer(http.FS(staticFS)))

	handleLogin := func(w http.ResponseWriter, r *http.Request) {
		sessionData := sessionFromContext(r.Context()).Load()

		var data struct {
			GiteaHostname string
			GiteaName     string
			SrhtUsername  string
			GiteaUsername string
			SrhtName      string
			EnableSecrets bool
		}
		if sessionData.SrhtToken == nil {
			data.SrhtName = config.Srht.Name
		} else {
			data.SrhtUsername = sessionData.SrhtToken.Username
		}
		if sessionData.GiteaToken == nil {
			data.GiteaHostname = giteaHostname
			data.GiteaName = config.Gitea.Name
		} else {
			data.GiteaUsername = sessionData.GiteaToken.Username
			if u, err := url.Parse(sessionData.GiteaToken.Issuer); err == nil {
				data.GiteaHostname = u.Host
			} else {
				data.GiteaHostname = sessionData.GiteaToken.Issuer
			}
			data.GiteaName = data.GiteaHostname
		}
		data.EnableSecrets = sessionData.EnableSecrets
		if err := tpl.ExecuteTemplate(w, "index.html", &data); err != nil {
			panic(err)
		}
	}

	handleDashboard := func(w http.ResponseWriter, r *http.Request) {
		sessionData := sessionFromContext(r.Context()).Load()
		giteaClient, err := newGiteaClient(r.Context(), sessionData.GiteaToken)
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to create Gitea client: %v", err), http.StatusInternalServerError)
			return
		}

		userHooks, err := listUserHooks(giteaClient, getOrigin(r))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var (
			repos   []*gitea.Repository
			page    int = 1
			maxPage int = -1
		)

		for maxPage == -1 || page <= maxPage {
			batch, resp, err := giteaClient.ListMyRepos(gitea.ListReposOptions{
				ListOptions: gitea.ListOptions{
					Page: page,
				},
			})
			if err != nil {
				http.Error(w, fmt.Sprintf("failed to list Gitea repositories: %v", err), http.StatusInternalServerError)
				return
			}
			if maxPage == -1 {
				maxPage = resp.LastPage
			}
			repos = append(repos, batch...)
			if resp.NextPage == 0 {
				break
			}
			page = resp.NextPage
		}

		giteaHostname := sessionData.GiteaToken.Issuer
		if u, err := url.Parse(sessionData.GiteaToken.Issuer); err == nil {
			giteaHostname = u.Host
		}

		data := struct {
			SrhtUsername  string
			SrhtName      string
			GiteaUsername string
			GiteaHostname string
			GiteaName     string
			UserEnabled   bool
			Repositories  []*gitea.Repository
		}{
			SrhtUsername:  sessionData.SrhtToken.Username,
			SrhtName:      config.Srht.Name,
			GiteaUsername: sessionData.GiteaToken.Username,
			GiteaHostname: giteaHostname,
			// TODO: Use the custom Gitea instance name rather than the default one, if applicable
			GiteaName:    config.Gitea.Name,
			UserEnabled:  len(userHooks) > 0,
			Repositories: repos,
		}
		if err := tpl.ExecuteTemplate(w, "dashboard.html", &data); err != nil {
			panic(err)
		}
	}

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		sessionData := sessionFromContext(r.Context()).Load()
		if sessionData.SrhtToken == nil || sessionData.GiteaToken == nil {
			handleLogin(w, r)
		} else {
			handleDashboard(w, r)
		}
	})

	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		var err error
		if r.Form.Has("disable_user") {
			err = disableUser(r.Context(), getOrigin(r))
		} else {
			err = enableUser(r.Context(), getOrigin(r))
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, r.URL.Path, http.StatusSeeOther)
	})

	r.Get("/logout", func(w http.ResponseWriter, r *http.Request) {
		sessionFromContext(r.Context()).Delete()
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	})

	redirectAuth := func(w http.ResponseWriter, r *http.Request, service string) {
		session := sessionFromContext(r.Context())
		origin := getOrigin(r)
		enableSecrets := r.URL.Query().Has("enable_secrets")

		sessionData := session.Load()
		sessionData.EnableSecrets = enableSecrets
		session.Store(sessionData)

		// copy auth and fill redirect URI
		var (
			auth  oauth2.Client
			scope []string
		)
		switch service {
		case "srht":
			auth = *srhtAuth
			scope = append(([]string)(nil), srhtAuthScope...)
			if enableSecrets {
				scope = append(scope, srhtSecretsScope)
			}
		case "gitea":
			auth = *giteaAuth
			scope = giteaAuthScope
		}
		auth.RedirectURI = origin + "/authorize-" + service

		redirectURL := auth.AuthorizationCodeURL(&oauth2.AuthorizationOptions{
			Scope: scope,
		})
		http.Redirect(w, r, redirectURL, http.StatusTemporaryRedirect)
	}

	r.Get("/redirect-srht", func(w http.ResponseWriter, r *http.Request) {
		redirectAuth(w, r, "srht")
	})

	r.Get("/redirect-gitea", func(w http.ResponseWriter, r *http.Request) {
		redirectAuth(w, r, "gitea")
	})

	r.Get("/authorize-srht", func(w http.ResponseWriter, r *http.Request) {
		session := sessionFromContext(r.Context())

		tokenResp, err := authorizeOAuth2(srhtAuth, r)
		if err != nil {
			http.Error(w, fmt.Sprintf("sr.ht auth error: %v", err), http.StatusBadRequest)
			return
		}

		gqlClient := newSrhtClientWithTokenResp(tokenResp)
		me, err := buildssrht.FetchUser(gqlClient, r.Context())
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to fetch sr.ht profile: %v", err), http.StatusBadRequest)
			return
		}

		token := newOAuthToken(config.Srht.MetaEndpoint, me.CanonicalName, tokenResp)
		if err := db.StoreOAuthToken(r.Context(), token); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		sessionData := session.Load()
		sessionData.SrhtToken = token
		session.Store(sessionData)

		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	})

	r.HandleFunc("/authorize-gitea", func(w http.ResponseWriter, r *http.Request) {
		session := sessionFromContext(r.Context())

		var (
			endpoint  string
			tokenResp *oauth2.TokenResp
		)
		if r.URL.Query().Has("custom") {
			endpoint = strings.TrimSuffix(r.FormValue("endpoint"), "/")
			tokenResp = &oauth2.TokenResp{
				TokenType:   oauth2.TokenTypeBearer,
				AccessToken: r.FormValue("token"),
			}

			if err := sanityCheckEndpoint(r.Context(), endpoint); err != nil {
				log.Printf("sanity check failed for Gitea endpoint %q: %v", endpoint, err)
				http.Error(w, "invalid endpoint", http.StatusBadRequest)
				return
			}
		} else {
			endpoint = config.Gitea.Endpoint

			var err error
			tokenResp, err = authorizeOAuth2(giteaAuth, r)
			if err != nil {
				http.Error(w, fmt.Sprintf("Gitea auth error: %v", err), http.StatusBadRequest)
				return
			}
		}

		giteaClient, err := newGiteaClientWithTokenResp(r.Context(), endpoint, tokenResp)
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to create Gitea client: %v", err), http.StatusInternalServerError)
			return
		}

		user, _, err := giteaClient.GetMyUserInfo()
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to fetch Gitea profile: %v", err), http.StatusBadRequest)
			return
		}

		token := newOAuthToken(endpoint, user.UserName, tokenResp)
		if err := db.StoreOAuthToken(r.Context(), token); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		sessionData := session.Load()
		sessionData.GiteaToken = token
		session.Store(sessionData)

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})

	r.Get("/repository/{id}", func(w http.ResponseWriter, r *http.Request) {
		repoID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			http.Error(w, "invalid repo ID", http.StatusBadRequest)
			return
		}

		sessionData := sessionFromContext(r.Context()).Load()
		giteaClient, err := newGiteaClient(r.Context(), sessionData.GiteaToken)
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to create Gitea client: %v", err), http.StatusInternalServerError)
			return
		}
		srhtClient, err := newSrhtClient(r.Context(), sessionData.SrhtToken)
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to create sr.ht client: %v", err), http.StatusInternalServerError)
			return
		}

		repo, _, err := giteaClient.GetRepoByID(repoID)
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to fetch repo: %v", err), http.StatusInternalServerError)
			return
		}

		installation, err := getRepoInstallation(r.Context(), giteaClient, repo.Owner.UserName, repo.Name, getOrigin(r))
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to get installation: %v", err), http.StatusInternalServerError)
			return
		}

		hasSecretsScope := false
		if installation != nil {
			for _, scope := range strings.Split(installation.SrhtToken.Scope, " ") {
				if scope == "builds.sr.ht/SECRETS:RO" {
					hasSecretsScope = true
					break
				}
			}
		}

		var srhtSecrets []buildssrht.Secret
		if hasSecretsScope {
			srhtSecrets, err = listSecrets(r.Context(), srhtClient)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		// TODO: check that the token has the builds.sr.ht/SECRETS:RO scope
		type secret struct {
			buildssrht.Secret
			Enabled bool
		}
		var secrets []secret
		for _, sec := range srhtSecrets {
			enabled := false
			if installation != nil {
				for _, uuid := range installation.Secrets {
					if uuid == sec.Uuid {
						enabled = true
						break
					}
				}
			}
			secrets = append(secrets, secret{Secret: sec, Enabled: enabled})
		}

		data := struct {
			Repository *gitea.Repository
			Enabled    bool
			Secrets    []secret
		}{
			Repository: repo,
			Enabled:    installation != nil,
			Secrets:    secrets,
		}
		if err := tpl.ExecuteTemplate(w, "repository.html", &data); err != nil {
			panic(err)
		}
	})

	r.Post("/repository/{id}", func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		owner := r.FormValue("owner")
		repo := r.FormValue("repo")
		enable := r.Form.Has("enable")
		disable := r.Form.Has("disable")

		var err error
		if disable {
			err = disableRepo(r.Context(), owner, repo, getOrigin(r))
		} else if enable {
			err = enableRepo(r.Context(), owner, repo, getOrigin(r))
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, r.URL.Path, http.StatusSeeOther)
	})

	r.Post("/repository/{id}/secrets", func(w http.ResponseWriter, r *http.Request) {
		repoID, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
		if err != nil {
			http.Error(w, "invalid repo ID", http.StatusBadRequest)
			return
		}

		r.ParseForm()
		owner := r.FormValue("owner")
		repo := r.FormValue("repo")
		secrets := r.Form["secrets"]

		sessionData := sessionFromContext(r.Context()).Load()
		giteaClient, err := newGiteaClient(r.Context(), sessionData.GiteaToken)
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to create Gitea client: %v", err), http.StatusInternalServerError)
			return
		}

		installation, err := getRepoInstallation(r.Context(), giteaClient, owner, repo, getOrigin(r))
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to get installation: %v", err), http.StatusInternalServerError)
			return
		} else if installation == nil {
			http.Error(w, "installation not found", http.StatusNotFound)
			return
		}

		installation.Secrets = secrets
		if err := db.StoreInstallation(r.Context(), installation); err != nil {
			http.Error(w, fmt.Sprintf("failed to store installation into DB: %v", err), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, fmt.Sprintf("/repository/%d", repoID), http.StatusSeeOther)
	})

	r.Post("/webhook", func(w http.ResponseWriter, r *http.Request) {
		// TODO: check X-Gitea-Signature

		installationToken := r.URL.Query().Get("installation_token")
		installation, err := db.GetInstallationWithWebhookToken(r.Context(), installationToken)
		if err != nil {
			http.Error(w, fmt.Sprintf("failed to get installation: %v", err), http.StatusBadRequest)
			return
		}

		switch event := r.Header.Get("X-Gitea-Event"); event {
		case "push":
			var data giteaCommitEvent
			if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			if err := handleCommit(r.Context(), &data, installation); err != nil {
				log.Printf("failed to handle commit webhook for installation %v: %v", installation.ID, err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		case "pull_request":
			var data giteaPullRequestEvent
			if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			if err := handlePullRequest(r.Context(), &data, installation); err != nil {
				log.Printf("failed to handle pull request webhook for installation %v: %v", installation.ID, err)
				return
			}
		default:
			http.Error(w, "unsupported event type", http.StatusBadRequest)
		}
	})

	server := http.Server{
		Addr:    config.Listen,
		Handler: r,
	}

	var cancelMonitor context.CancelFunc
	monitorContext, cancelMonitor = context.WithCancel(context.Background())

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigCh

		cancelMonitor()

		log.Printf("Shutting down server")
		if err := server.Shutdown(context.Background()); err != nil {
			log.Fatalf("failed to shutdown server: %v", err)
		}
	}()

	log.Printf("HTTP server listening on %v", server.Addr)
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("failed to listen and serve: %v", err)
	}

	// By this point, no more incoming HTTP requests are handled
	monitorWaitGroup.Wait()
}

func authorizeOAuth2(oauthClient *oauth2.Client, r *http.Request) (*oauth2.TokenResp, error) {
	authResp, err := oauthClient.ParseAuthorizationCodeResp(r.URL)
	if err != nil {
		return nil, err
	}
	return oauthClient.Exchange(r.Context(), authResp.Code)
}

func getOrigin(r *http.Request) string {
	return getForwardedProto(r.Header) + "://" + r.Host
}

func getForwardedProto(h http.Header) string {
	if forwarded := h.Get("Forwarded"); forwarded != "" {
		_, params, _ := mime.ParseMediaType("_;" + forwarded)
		if proto := params["proto"]; proto == "https" {
			return "https"
		}
	}
	if proto := h.Get("X-Forwarded-Proto"); proto == "https" {
		return "https"
	}
	return "http"
}

func refreshOAuthToken(ctx context.Context, client *oauth2.Client, token *OAuthToken) error {
	if token.RefreshToken == "" || token.ExpiresAt.IsZero() {
		return nil
	}
	if time.Now().Add(5 * time.Minute).Before(token.ExpiresAt) {
		return nil
	}

	resp, err := client.Refresh(ctx, token.RefreshToken, nil)
	if err != nil {
		return fmt.Errorf("failed to refresh token: %v", err)
	}

	newToken := newOAuthToken(token.Issuer, token.Username, resp)
	if err := db.StoreOAuthToken(ctx, newToken); err != nil {
		return fmt.Errorf("failed to store fresh token: %v", err)
	}

	*token = *newToken
	return nil
}

func newSrhtClientWithTokenResp(tokenResp *oauth2.TokenResp) *gqlclient.Client {
	httpClient := srhtAuth.NewHTTPClient(tokenResp)
	return gqlclient.New(config.Srht.BuildsEndpoint+"/query", httpClient)
}

func newSrhtClient(ctx context.Context, token *OAuthToken) (*gqlclient.Client, error) {
	if token == nil {
		return nil, fmt.Errorf("missing sr.ht token")
	}
	if err := refreshOAuthToken(ctx, srhtAuth, token); err != nil {
		return nil, err
	}
	return newSrhtClientWithTokenResp(token.tokenResp()), nil
}

func newGiteaClientWithTokenResp(ctx context.Context, endpoint string, tokenResp *oauth2.TokenResp) (*gitea.Client, error) {
	httpClient := giteaAuth.NewHTTPClient(tokenResp)
	giteaClient, err := gitea.NewClient(endpoint, gitea.SetHTTPClient(httpClient), gitea.SetContext(ctx))
	if err != nil {
		// This can happen on invalid token, because the library checks the
		// server version
		return nil, err
	}
	return giteaClient, nil
}

func newGiteaClient(ctx context.Context, token *OAuthToken) (*gitea.Client, error) {
	if token == nil {
		return nil, fmt.Errorf("missing gitea token")
	}
	if err := refreshOAuthToken(ctx, giteaAuth, token); err != nil {
		return nil, err
	}
	return newGiteaClientWithTokenResp(ctx, token.Issuer, token.tokenResp())
}

func getHookEndpoint(origin string) string {
	if webhookEndpoint != "" {
		return webhookEndpoint
	}
	return origin + "/webhook"
}

func filterHooks(hooks []*gitea.Hook, origin string) []*gitea.Hook {
	hookURLPrefix := getHookEndpoint(origin) + "?"
	var ourHooks []*gitea.Hook
	for _, hook := range hooks {
		url := hook.Config["url"] // for some reason hook.URL is empty
		if gitea.HookType(hook.Type) == gitea.HookTypeGitea && strings.HasPrefix(url, hookURLPrefix) {
			ourHooks = append(ourHooks, hook)
		}
	}
	return ourHooks
}

func prepareHook(ctx context.Context, sessionData *sessionData, origin string) (*gitea.CreateHookOption, error) {
	installation := &Installation{
		SrhtToken:  *sessionData.SrhtToken,
		GiteaToken: *sessionData.GiteaToken,
	}
	if err := db.StoreInstallation(ctx, installation); err != nil {
		return nil, fmt.Errorf("failed to store installation into DB: %v", err)
	}

	return &gitea.CreateHookOption{
		Type: gitea.HookTypeGitea,
		Config: map[string]string{
			"url":          getHookEndpoint(origin) + "?installation_token=" + installation.WebhookToken,
			"content_type": "json",
			"http_method":  "post",
		},
		Events:       []string{"push", "pull_request_only", "pull_request_sync"},
		BranchFilter: "*",
		Active:       true,
	}, nil
}

func listRepoHooks(giteaClient *gitea.Client, owner, repo, origin string) ([]*gitea.Hook, error) {
	hooks, _, err := giteaClient.ListRepoHooks(owner, repo, gitea.ListHooksOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to fetch Gitea repo webhooks: %v", err)
	}
	return filterHooks(hooks, origin), nil
}

func getRepoInstallation(ctx context.Context, giteaClient *gitea.Client, owner, repo, origin string) (*Installation, error) {
	hooks, err := listRepoHooks(giteaClient, owner, repo, origin)
	if err != nil {
		return nil, err
	}

	webhookToken := ""
	for _, hook := range hooks {
		if hook.Active {
			u, _ := url.Parse(hook.Config["url"])
			if u != nil {
				webhookToken = u.Query().Get("installation_token")
				break
			}
		}
	}
	if webhookToken == "" {
		return nil, nil
	}

	installation, err := db.GetInstallationWithWebhookToken(ctx, webhookToken)
	if err != nil && err != ErrNotFound {
		return nil, err
	}

	return installation, nil
}

func enableRepo(ctx context.Context, owner, repo, origin string) error {
	sessionData := sessionFromContext(ctx).Load()
	giteaClient, err := newGiteaClient(ctx, sessionData.GiteaToken)
	if err != nil {
		return fmt.Errorf("failed to create Gitea client: %v", err)
	}

	hookOptions, err := prepareHook(ctx, sessionData, origin)
	if err != nil {
		return err
	}

	_, _, err = giteaClient.CreateRepoHook(owner, repo, *hookOptions)
	if err != nil {
		return fmt.Errorf("failed to create repo hook: %v", err)
	}

	return nil
}

func disableRepo(ctx context.Context, owner, repo, origin string) error {
	sessionData := sessionFromContext(ctx).Load()
	giteaClient, err := newGiteaClient(ctx, sessionData.GiteaToken)
	if err != nil {
		return fmt.Errorf("failed to create Gitea client: %v", err)
	}

	hooks, err := listRepoHooks(giteaClient, owner, repo, origin)
	if err != nil {
		return err
	}

	for _, hook := range hooks {
		_, err := giteaClient.DeleteRepoHook(owner, repo, hook.ID)
		if err != nil {
			return fmt.Errorf("failed to delete Gitea repo webhook: %v", err)
		}
	}

	// TODO: delete installation from DB as well (but old installations may be
	// shared between multiple repositories/users)
	return nil
}

func listUserHooks(giteaClient *gitea.Client, origin string) ([]*gitea.Hook, error) {
	hooks, _, err := giteaClient.ListMyHooks(gitea.ListHooksOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to fetch Gitea user webhooks: %v", err)
	}
	return filterHooks(hooks, origin), nil
}

func enableUser(ctx context.Context, origin string) error {
	sessionData := sessionFromContext(ctx).Load()
	giteaClient, err := newGiteaClient(ctx, sessionData.GiteaToken)
	if err != nil {
		return fmt.Errorf("failed to create Gitea client: %v", err)
	}

	hookOptions, err := prepareHook(ctx, sessionData, origin)
	if err != nil {
		return err
	}

	_, _, err = giteaClient.CreateMyHook(*hookOptions)
	if err != nil {
		return fmt.Errorf("failed to create user hook: %v", err)
	}

	return nil
}

func disableUser(ctx context.Context, origin string) error {
	sessionData := sessionFromContext(ctx).Load()
	giteaClient, err := newGiteaClient(ctx, sessionData.GiteaToken)
	if err != nil {
		return fmt.Errorf("failed to create Gitea client: %v", err)
	}

	hooks, err := listUserHooks(giteaClient, origin)
	if err != nil {
		return err
	}

	for _, hook := range hooks {
		_, err := giteaClient.DeleteMyHook(hook.ID)
		if err != nil {
			return fmt.Errorf("failed to delete Gitea user webhook: %v", err)
		}
	}

	// TODO: delete installation from DB as well (but old installations may be
	// shared between multiple repositories/users)
	return nil
}

func listSecrets(ctx context.Context, c *gqlclient.Client) ([]buildssrht.Secret, error) {
	var (
		secrets []buildssrht.Secret
		cursor  *buildssrht.Cursor
	)
	for {
		data, err := buildssrht.FetchSecrets(c, ctx, cursor)
		if err != nil {
			return secrets, err
		}
		secrets = append(secrets, data.Results...)
		cursor = data.Cursor
		if cursor == nil {
			break
		}
	}
	return secrets, nil
}

func sanityCheckEndpoint(ctx context.Context, endpoint string) error {
	u, err := url.Parse(endpoint)
	if err != nil {
		return err
	}
	if u.Scheme != "https" {
		return fmt.Errorf("scheme must be HTTPS")
	}

	ips, err := net.DefaultResolver.LookupIP(ctx, "ip", u.Host)
	if err != nil {
		return fmt.Errorf("DNS lookup failed: %v", err)
	}

	for _, ip := range ips {
		if ip.IsLoopback() || ip.IsMulticast() || ip.IsPrivate() {
			return fmt.Errorf("invalid IP %v", ip)
		}
	}

	return nil
}
