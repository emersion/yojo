package main

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"strings"

	"code.gitea.io/sdk/gitea"
	"gopkg.in/yaml.v3"
)

type giteaRepository struct {
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	FullName string `json:"full_name"`
	CloneURL string `json:"clone_url"`
	Owner    struct {
		Username string `json:"username"`
	} `json:"owner"`
	Private  bool `json:"private"`
	Internal bool `json:"internal"`
}

type giteaCommit struct {
	ID      string `json:"id"`
	Message string `json:"message"`
	URL     string `json:"url"`
	Author  *struct {
		Name     string `json:"name"`
		Username string `json:"username"`
	} `json:"author"`
}

type giteaCommitEvent struct {
	Ref        string           `json:"ref"`
	HeadCommit *giteaCommit     `json:"head_commit"`
	Repository *giteaRepository `json:"repository"`
}

type giteaPullRequest struct {
	Number int    `json:"number"`
	Title  string `json:"title"`
	URL    string `json:"url"`
	User   *struct {
		Username string `json:"username"`
	} `json:"user"`
	Head *struct {
		SHA        string           `json:"sha"`
		Repository *giteaRepository `json:"repo"`
	} `json:"head"`
	Base *struct {
		Repository *giteaRepository `json:"repo"`
	} `json:"base"`
}

type giteaPullRequestEvent struct {
	Action      string            `json:"action"`
	PullRequest *giteaPullRequest `json:"pull_request"`
}

func fetchManifest(client *gitea.Client, owner, repo, ref, filename string) (map[string]interface{}, error) {
	contents, resp, err := client.GetContents(owner, repo, ref, filename)
	if resp.Response.StatusCode == http.StatusNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	} else if contents.Type != "file" {
		return nil, fmt.Errorf("not a file")
	} else if *contents.Encoding != "base64" {
		return nil, fmt.Errorf("unsupported encoding %q", *contents.Encoding)
	}

	b, err := base64.StdEncoding.DecodeString(*contents.Content)
	if err != nil {
		return nil, fmt.Errorf("malformed content: %v", err)
	}

	var manifest map[string]interface{}
	if err := yaml.Unmarshal(b, &manifest); err != nil {
		return nil, fmt.Errorf("failed to parse manifest: %v", err)
	}

	return manifest, nil
}

func fetchManifestFilenames(client *gitea.Client, owner, repo, ref string) ([]string, error) {
	l, resp, err := client.ListContents(owner, repo, ref, ".builds")
	if resp.Response.StatusCode == http.StatusNotFound {
		return []string{".build.yml"}, nil
	} else if err != nil {
		return nil, err
	}

	var filenames []string
	for _, file := range l {
		if file.Type != "file" {
			continue
		}
		if !strings.HasSuffix(file.Name, ".yml") && !strings.HasSuffix(file.Name, ".yaml") {
			continue
		}
		filenames = append(filenames, file.Path)
	}

	return filenames, nil
}
