module codeberg.org/emersion/yojo

go 1.20

require (
	code.gitea.io/sdk/gitea v0.19.0
	git.sr.ht/~emersion/go-oauth2 v0.0.0-20240226120011-78f10ffd1d51
	git.sr.ht/~emersion/go-scfg v0.0.0-20240128091534-2ae16e782082
	git.sr.ht/~emersion/gqlclient v0.0.0-20230820050442-8873fe0204b9
	github.com/go-chi/chi/v5 v5.1.0
	github.com/mattn/go-sqlite3 v1.14.24
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/dave/jennifer v1.7.1 // indirect
	github.com/davidmz/go-pageant v1.0.2 // indirect
	github.com/go-fed/httpsig v1.1.0 // indirect
	github.com/hashicorp/go-version v1.7.0 // indirect
	github.com/vektah/gqlparser/v2 v2.5.17 // indirect
	golang.org/x/crypto v0.29.0 // indirect
	golang.org/x/sys v0.27.0 // indirect
)
