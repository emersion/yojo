package main

import (
	"context"
	"database/sql"
	_ "embed"
	"fmt"
	"strings"
	"time"

	"git.sr.ht/~emersion/go-oauth2"
	_ "github.com/mattn/go-sqlite3"
)

var ErrNotFound = sql.ErrNoRows

//go:embed schema.sql
var schema string

var migrations = []string{
	"", // migration #0 is reserved for schema initialization
	`
		CREATE TABLE OAuthToken (
			id INTEGER PRIMARY KEY,
			issuer TEXT NOT NULL,
			username TEXT NOT NULL,
			access_token TEXT NOT NULL,
			refresh_token TEXT,
			created_at datetime NOT NULL,
			expires_at datetime
		);

		CREATE UNIQUE INDEX OAuthTokenIndex ON OAuthToken(issuer, username);

		INSERT INTO OAuthToken (
			issuer,
			username,
			access_token,
			created_at
		) SELECT DISTINCT 'https://meta.sr.ht', srht_username, srht_access_token, created_at
		FROM Installation;

		INSERT INTO OAuthToken (
			issuer,
			username,
			access_token,
			created_at
		) SELECT DISTINCT gitea_endpoint, gitea_username, gitea_access_token, created_at
		FROM Installation;

		ALTER TABLE Installation ADD COLUMN srht_token INTEGER REFERENCES OAuthToken(id);
		ALTER TABLE Installation ADD COLUMN gitea_token INTEGER REFERENCES OAuthToken(id);

		UPDATE Installation
		SET srht_token = (
			SELECT id
			FROM OAuthToken
			WHERE issuer = 'https://meta.sr.ht' AND
				username = srht_username AND
				access_token = srht_access_token
		);

		UPDATE Installation
		SET gitea_token = (
			SELECT id
			FROM OAuthToken
			WHERE issuer = gitea_endpoint AND
				username = gitea_username AND
				access_token = gitea_access_token
		);

		ALTER TABLE Installation DROP COLUMN srht_access_token;
		ALTER TABLE Installation DROP COLUMN srht_username;
		ALTER TABLE Installation DROP COLUMN gitea_access_token;
		ALTER TABLE Installation DROP COLUMN gitea_username;
		ALTER TABLE Installation DROP COLUMN gitea_endpoint;
	`,
	`
		CREATE UNIQUE INDEX InstallationWebhookTokenIndex ON Installation(webhook_token);
	`,
	`
		ALTER TABLE OAuthToken ADD COLUMN scope TEXT;
	`,
	`
		ALTER TABLE Installation ADD COLUMN secrets TEXT NOT NULL DEFAULT '';
	`,
}

type Installation struct {
	ID           int64
	WebhookToken string
	SrhtToken    OAuthToken
	GiteaToken   OAuthToken
	CreatedAt    time.Time
	Secrets      []string
}

type OAuthToken struct {
	ID           int64
	Issuer       string
	Username     string
	AccessToken  string
	RefreshToken string
	CreatedAt    time.Time
	ExpiresAt    time.Time
	Scope        string
}

func newOAuthToken(issuer, username string, resp *oauth2.TokenResp) *OAuthToken {
	var expiresAt time.Time
	if resp.ExpiresIn > 0 {
		expiresAt = time.Now().Add(resp.ExpiresIn)
	}

	return &OAuthToken{
		Issuer:       issuer,
		Username:     username,
		AccessToken:  resp.AccessToken,
		RefreshToken: resp.RefreshToken,
		ExpiresAt:    expiresAt,
		Scope:        strings.Join(resp.Scope, " "),
	}
}

func (token *OAuthToken) tokenResp() *oauth2.TokenResp {
	return &oauth2.TokenResp{
		TokenType:   oauth2.TokenTypeBearer,
		AccessToken: token.AccessToken,
	}
}

type DB struct {
	db *sql.DB
}

func openDB(filename string) (*DB, error) {
	// Open the DB with cache=shared and SetMaxOpenConns(1) to allow usage from
	// multiple goroutines
	sqlDB, err := sql.Open("sqlite3", filename+"?cache=shared")
	if err != nil {
		return nil, err
	}
	sqlDB.SetMaxOpenConns(1)
	db := &DB{sqlDB}
	if err := db.upgrade(); err != nil {
		db.Close()
		return nil, fmt.Errorf("failed to upgrade DB: %v", err)
	}
	return db, nil
}

func (db *DB) Close() error {
	return db.db.Close()
}

func (db *DB) upgrade() error {
	var version int
	if err := db.db.QueryRow("PRAGMA user_version").Scan(&version); err != nil {
		return fmt.Errorf("failed to query schema version: %v", err)
	}

	if version == len(migrations) {
		return nil
	} else if version > len(migrations) {
		return fmt.Errorf("yojo (version %d) older than schema (version %d)", len(migrations), version)
	}

	tx, err := db.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if version == 0 {
		if _, err := tx.Exec(schema); err != nil {
			return fmt.Errorf("failed to initialize schema: %v", err)
		}
	} else {
		for i := version; i < len(migrations); i++ {
			if _, err := tx.Exec(migrations[i]); err != nil {
				return fmt.Errorf("failed to execute migration #%v: %v", i, err)
			}
		}
	}

	// For some reason prepared statements don't work here
	_, err = tx.Exec(fmt.Sprintf("PRAGMA user_version = %d", len(migrations)))
	if err != nil {
		return fmt.Errorf("failed to bump schema version: %v", err)
	}

	return tx.Commit()
}

func (db *DB) StoreOAuthToken(ctx context.Context, token *OAuthToken) error {
	args := []interface{}{
		sql.Named("issuer", token.Issuer),
		sql.Named("username", token.Username),
		sql.Named("access_token", token.AccessToken),
		sql.Named("refresh_token", token.RefreshToken),
		sql.Named("expires_at", token.ExpiresAt),
		sql.Named("now", time.Now()),
		sql.Named("scope", sql.NullString{String: token.Scope, Valid: token.Scope != ""}),
	}

	return db.db.QueryRowContext(ctx, `
		INSERT INTO OAuthToken (issuer, username, access_token, refresh_token,
			created_at, expires_at, scope)
		VALUES (:issuer, :username, :access_token, :refresh_token,
			:now, :expires_at, :scope)
		ON CONFLICT (issuer, username)
		DO UPDATE SET access_token = :access_token,
			refresh_token = :refresh_token,
			expires_at = :expires_at,
			scope = :scope
		RETURNING id, created_at;
	`, args...).Scan(&token.ID, &token.CreatedAt)
}

func (db *DB) StoreInstallation(ctx context.Context, installation *Installation) error {
	if installation.SrhtToken.ID == 0 {
		panic("installation with sr.ht token missing from DB")
	}
	if installation.GiteaToken.ID == 0 {
		panic("installation with Gitea token missing from DB")
	}

	tx, err := db.db.BeginTx(ctx, nil)
	if err != nil {
		return nil
	}
	defer tx.Rollback()

	if installation.WebhookToken == "" {
		installation.WebhookToken, err = generateToken()
		if err != nil {
			return err
		}
	}

	args := []interface{}{
		sql.Named("id", installation.ID),
		sql.Named("webhook_token", installation.WebhookToken),
		sql.Named("srht_token", installation.SrhtToken.ID),
		sql.Named("gitea_token", installation.GiteaToken.ID),
		sql.Named("now", time.Now()),
		sql.Named("secrets", strings.Join(installation.Secrets, "\n")),
	}

	if installation.ID != 0 {
		_, err = tx.Exec(`
			UPDATE Installation
			SET
				webhook_token = :webhook_token,
				srht_token = :srht_token,
				gitea_token = :gitea_token,
				secrets = :secrets
			WHERE id = :id
		`, args...)
	} else {
		var res sql.Result
		res, err = tx.Exec(`
			INSERT INTO Installation(
				webhook_token,
				srht_token,
				gitea_token,
				created_at,
				secrets
			)
			VALUES (
				:webhook_token,
				:srht_token,
				:gitea_token,
				:now,
				:secrets
			)
		`, args...)
		if err != nil {
			return err
		}
		installation.ID, err = res.LastInsertId()
	}
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (db *DB) GetInstallationWithWebhookToken(ctx context.Context, webhookToken string) (*Installation, error) {
	var (
		inst       Installation
		srhtScope  sql.NullString
		giteaScope sql.NullString
		secrets    string
	)
	row := db.db.QueryRowContext(ctx, `
		SELECT
			inst.id,
			inst.webhook_token,
			inst.created_at,
			inst.secrets,
			srht.id,
			srht.issuer,
			srht.username,
			srht.access_token,
			srht.refresh_token,
			srht.created_at,
			srht.expires_at,
			srht.scope,
			gitea.id,
			gitea.issuer,
			gitea.username,
			gitea.access_token,
			gitea.refresh_token,
			gitea.created_at,
			gitea.expires_at,
			gitea.scope
		FROM Installation inst, OAuthToken srht, OAuthToken gitea
		WHERE webhook_token = ? AND
			inst.srht_token = srht.id AND
			inst.gitea_token = gitea.id
	`, webhookToken)
	err := row.Scan(
		&inst.ID,
		&inst.WebhookToken,
		&inst.CreatedAt,
		&secrets,
		&inst.SrhtToken.ID,
		&inst.SrhtToken.Issuer,
		&inst.SrhtToken.Username,
		&inst.SrhtToken.AccessToken,
		&inst.SrhtToken.RefreshToken,
		&inst.SrhtToken.CreatedAt,
		&inst.SrhtToken.ExpiresAt,
		&srhtScope,
		&inst.GiteaToken.ID,
		&inst.GiteaToken.Issuer,
		&inst.GiteaToken.Username,
		&inst.GiteaToken.AccessToken,
		&inst.GiteaToken.RefreshToken,
		&inst.GiteaToken.CreatedAt,
		&inst.GiteaToken.ExpiresAt,
		&giteaScope,
	)
	inst.SrhtToken.Scope = srhtScope.String
	inst.GiteaToken.Scope = giteaScope.String
	if secrets != "" {
		inst.Secrets = strings.Split(secrets, "\n")
	}
	return &inst, err
}
