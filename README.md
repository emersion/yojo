# yojo

A CI bridge from Forgejo/Gitea to SourceHut.

A [public instance] is available.

## Configuration

yojo can be configured via an [scfg] file by running `yojo -config <path>`.

Only the OAuth client ID and secrets are required.

* For sr.ht, [register an sr.ht OAuth client] with the redirect URL set to
  `https://<origin>/authorize-srht`.
* For Codeberg, [register a Codeberg OAuth client] with the redirect URL set to
  `https://<origin>/authorize-gitea`.

```
# HTTP server listen address
listen localhost:8080
# SQLite database path
db yojo.db

# SourceHut configuration
srht {
	# OAuth client ID and secret
	client-id "..."
	client-secret "..."

	# Endpoint URLs for builds.sr.ht and meta.sr.ht
	builds-endpoint "https://builds.sr.ht"
	meta-endpoint "https://meta.sr.ht"
}

# Gitea/Forgejo configuration
gitea {
	# OAuth client ID and secret
	client-id "..."
	client-secret "..."

	# Endpoint URL
	endpoint "https://codeberg.org"
}
```

## Local development

For local development, [smee.io] can be used to forward Web hooks. Install
[go-smee][] (or their offical client), then run:

    smee -target "http://localhost:8080/webhook"

Grab the channel URL and start yojo like so:

    yojo -config <config> -webhook-endpoint "https://smee.io/<token>"

To restart the smee client with the same channel, `-proxy` can be used:

    smee -target "http://localhost:8080/webhook" -proxy "https://smee.io/<token>"

## License

AGPLv3, see LICENSE.

Copyright (C) 2023 Simon Ser

[public instance]: https://yojo.emersion.fr
[scfg]: https://git.sr.ht/~emersion/scfg
[register an sr.ht OAuth client]: https://meta.sr.ht/oauth2/client-registration
[register a Codeberg OAuth client]: https://codeberg.org/user/settings/applications
[smee.io]: https://smee.io/
[go-smee]: https://git.sr.ht/~emersion/go-smee
