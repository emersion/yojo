//go:build generate

package buildssrht

import (
	_ "git.sr.ht/~emersion/gqlclient/cmd/gqlclientgen"
)

//go:generate curl -o schema.graphqls https://git.sr.ht/~sircmpwn/builds.sr.ht/blob/master/api/graph/schema.graphqls
//go:generate go run git.sr.ht/~emersion/gqlclient/cmd/gqlclientgen -s schema.graphqls -q operations.graphql -o gql.go
