package main

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"net/http"
	"sync"
	"sync/atomic"
	"time"
)

const (
	sessionCookieName = "yojo_session"
	sessionTimeout    = 2 * time.Hour
)

type sessionContextKey struct{}

type sessionHandler struct {
	next http.Handler

	mutex sync.Mutex
	m     map[string]*session
}

func sessionMiddleware(next http.Handler) http.Handler {
	sh := &sessionHandler{
		next: next,
		m:    make(map[string]*session),
	}
	go sh.pruneLoop()
	return sh
}

func (sh *sessionHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	session, err := sh.session(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ctx := r.Context()
	ctx = context.WithValue(ctx, sessionContextKey{}, session)
	r = r.WithContext(ctx)
	sh.next.ServeHTTP(w, r)
}

func (sh *sessionHandler) session(w http.ResponseWriter, r *http.Request) (*session, error) {
	cookie, err := r.Cookie(sessionCookieName)
	if err == http.ErrNoCookie {
		return sh.createSession(w)
	} else if err != nil {
		return nil, err
	}

	sh.mutex.Lock()
	session := sh.m[cookie.Value]
	if session != nil {
		session.t = time.Now()
	}
	sh.mutex.Unlock()

	if session == nil {
		return sh.createSession(w)
	}
	return session, nil
}

func (sh *sessionHandler) createSession(w http.ResponseWriter) (*session, error) {
	k, err := generateToken()
	if err != nil {
		return nil, err
	}

	session := &session{handler: sh, k: k, t: time.Now()}
	session.Store(new(sessionData))

	sh.mutex.Lock()
	sh.m[k] = session
	sh.mutex.Unlock()

	http.SetCookie(w, &http.Cookie{
		Name:     sessionCookieName,
		Value:    k,
		HttpOnly: true,
		Path:     "/",
	})

	return session, nil
}

func (sh *sessionHandler) pruneLoop() {
	ticker := time.NewTicker(sessionTimeout)
	for range ticker.C {
		sh.prune()
	}
}

func (sh *sessionHandler) prune() {
	sh.mutex.Lock()
	defer sh.mutex.Unlock()

	deadline := time.Now().Add(-sessionTimeout)
	var pruned []string
	for k, session := range sh.m {
		if session.t.Before(deadline) {
			pruned = append(pruned, k)
		}
	}

	for _, k := range pruned {
		delete(sh.m, k)
	}
}

type session struct {
	handler *sessionHandler
	k       string
	t       time.Time    // guarded by sessionHandler.mutex
	v       atomic.Value // sessionData
}

func sessionFromContext(ctx context.Context) *session {
	return ctx.Value(sessionContextKey{}).(*session)
}

func (s *session) Load() *sessionData {
	return s.v.Load().(*sessionData)
}

func (s *session) Store(data *sessionData) {
	copy := *data
	s.v.Store(&copy)
}

func (s *session) Delete() {
	s.handler.mutex.Lock()
	delete(s.handler.m, s.k)
	s.handler.mutex.Unlock()
}

type sessionData struct {
	SrhtToken, GiteaToken *OAuthToken
	EnableSecrets         bool
}

func generateToken() (string, error) {
	var buf [32]byte
	if _, err := rand.Read(buf[:]); err != nil {
		return "", err
	}
	return hex.EncodeToString(buf[:]), nil
}
